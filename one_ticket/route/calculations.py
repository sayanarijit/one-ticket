class CreditConverter(object):

    inr_rate = 1

    def from_inr(self, inr):
        """Converts INR to credit points"""

        return self.inr_rate * inr


class CreditCalculator(object):

    distance_rate = 1
    duration_rate = 1
    cummute_rate = {
        'bus': 1,
        'train': 1,
        'metro': 1.1,
        'cab': 1.2
    }

    def calculate(self, distance, duration, commute_type):
        """Calculates rate based on distance, commute and duration"""

        return (
            ((self.distance_rate * distance + self.duration_rate * duration) / 2)
            * self.cummute_rate[commute_type]
        )

credit_converter = CreditConverter()
credit_calculator = CreditCalculator()
