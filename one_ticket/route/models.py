import uuid
from django.db import models

from profiles.models import UserProfile


class RouteGenerator(models.Model):
    """RouteGenerator model"""

    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(UserProfile, on_delete=models.CASCADE)

    source_name = models.TextField()
    destination_name = models.TextField()

    def __str__(self):
        return '{}/{}: {} -> {}'.format(
            self.user_id, self.id, self.source_name, self.destination_name
        )
    

class Route(models.Model):
    """Route model"""

    id = models.AutoField(primary_key=True)
    option_id = models.ForeignKey(
        RouteGenerator, related_name='routes', on_delete=models.CASCADE)

    source_lat = models.FloatField(blank=False)
    source_lng = models.FloatField(blank=False)
    source_name = models.TextField(blank=False)

    destination_lat = models.FloatField(blank=False)
    destination_lng = models.FloatField(blank=False)
    destination_name = models.TextField(blank=False)

    credit = models.FloatField(blank=False)
    duration = models.IntegerField(blank=False)
    distance = models.IntegerField(blank=False)

    class Meta:
        ordering = ('duration', 'credit')

    def __str__(self):
        return '{}/{}: {} -> {}'.format(
            self.option_id, self.id, self.source_name, self.destination_name
        )


class RouteStep(models.Model):
    """RouteStep model"""

    COMMUTE_TYPES = {
        'bus': 'Bus',
        'metro': 'Metro',
        'cab': 'Cab',
        'train': 'Train'
    }
    id = models.IntegerField(primary_key=True)
    route_id = models.ForeignKey(Route, related_name='steps', on_delete=models.CASCADE)

    source_lat = models.FloatField(blank=False)
    source_lng = models.FloatField(blank=False)
    source_name = models.TextField()

    destination_lat = models.FloatField(blank=False)
    destination_lng = models.FloatField(blank=False)
    destination_name = models.TextField()

    credit = models.FloatField(blank=False)
    duration = models.IntegerField(blank=False)
    distance = models.IntegerField(blank=False)
    commute_type = models.TextField(blank=False, choices=COMMUTE_TYPES.items())
    number = models.TextField(max_length=255, blank=True)

    class Meta:
        ordering = ('route_id', 'id')

    def __str__(self):
        return '{}/{}'.format(
            self.route_id, self.id
        )
