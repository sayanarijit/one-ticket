from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views


router = DefaultRouter()
router.register('generate', views.RouteGeneratorViewSet)
router.register('options', views.RoutesViewSet)
router.register('steps', views.RouteStepsViewSet)


urlpatterns = [
    path('', include(router.urls))
]
