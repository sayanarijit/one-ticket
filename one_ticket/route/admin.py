from django.contrib import admin

from . import models

admin.site.register(models.RouteGenerator)
admin.site.register(models.Route)
admin.site.register(models.RouteStep)
