import uuid
from datetime import datetime
from rest_framework import serializers

from . import models
from .calculations import credit_converter, credit_calculator
from .map_clients import gmaps


class RouteGeneratorSerializer(serializers.ModelSerializer):
    """A serializer for RouteGenerator model"""

    routes = serializers.PrimaryKeyRelatedField(
        many=True, read_only=True
    )

    class Meta:
        model = models.RouteGenerator
        fields = ('user_id', 'routes', 'source_name', 'destination_name')
        read_only_fields = ('user_id', 'routes')
    
    def create(self, validated_data):
        """Generate and return new routes"""

        models.RouteGenerator.objects.filter(
            user_id=validated_data['user_id']).delete()

        directions_result = gmaps.directions(validated_data['source_name'],
                                             validated_data['destination_name'],
                                             mode="transit", alternatives=True,
                                             departure_time=datetime.now())
        options = models.RouteGenerator(
            user_id = validated_data['user_id'],
            source_name = validated_data['source_name'],
            destination_name = validated_data['destination_name'],
        )
        options.save()
        if len(directions_result) == 0:
            return options

        for result in directions_result:
            if len(result['legs']) == 0: continue
            leg = result['legs'][0]
            route = models.Route(
                option_id=options,

                source_lat=leg['start_location']['lat'],
                source_lng=leg['start_location']['lng'],
                source_name=leg['start_address'],

                destination_lat=leg['end_location']['lat'],
                destination_lng=leg['end_location']['lng'],
                destination_name=leg['end_address'],

                credit=0,
                duration=leg['duration']['value'],
                distance=leg['distance']['value']
            )
            route.save()
            for step in leg['steps']:
                if 'transit_details' not in step:
                    continue
                try:
                    commute_type = step['transit_details']['line']['vehicle']['name'].lower()
                    source_lat = step['transit_details']['departure_stop']['location']['lat']
                    source_lng = step['transit_details']['departure_stop']['location']['lng']
                    source_name = step['transit_details']['departure_stop']['name']
                    destination_lat = step['transit_details']['arrival_stop']['location']['lat']
                    destination_lng = step['transit_details']['arrival_stop']['location']['lng']
                    destination_name = step['transit_details']['arrival_stop']['name']
                except:
                    continue

                if commute_type not in credit_calculator.cummute_rate:
                    continue
                
                step = models.RouteStep(
                    route_id=route,

                    source_lat=source_lat,
                    source_lng=source_lng,
                    source_name=source_name,

                    destination_lat=destination_lat,
                    destination_lng=destination_lng,
                    destination_name=destination_name,

                    credit=0,
                    duration=step['duration']['value'],
                    distance=step['distance']['value'],
                    commute_type=commute_type
                )
                step.credit = credit_calculator.calculate(
                    distance=step.distance, duration=step.duration,
                    commute_type=commute_type)
                route.credit += step.credit
                step.save()
            route.save()
        return options

class RouteSerializer(serializers.ModelSerializer):
    """A serializer for Route object"""

    steps = serializers.PrimaryKeyRelatedField(
        many=True, read_only=True)

    class Meta:
        model = models.Route
        fields = ('id', 'option_id', 'source_lat',
                  'source_lng', 'source_name', 'destination_lat',
                  'destination_lng', 'destination_name',
                  'credit', 'duration', 'distance', 'steps')
        read_only_fields = ('id', 'option_id', 'source_lat',
                            'source_lng', 'source_name', 'destination_lat',
                            'destination_lng', 'destination_name',
                            'credit', 'duration', 'distance', 'steps')


class RouteStepSerializer(serializers.ModelSerializer):
    """A serializer for RouteStep object"""

    class Meta:
        model = models.RouteStep
        fields = ('id', 'route_id', 'source_lat', 'source_lng', 'source_name',
                  'destination_lat', 'destination_lng', 'destination_name',
                  'credit', 'duration', 'distance', 'commute_type', 'number')
        read_only_fields = ('id', 'route_id', 'source_lat', 'source_lng', 'source_name',
                            'destination_lat', 'destination_lng', 'destination_name',
                            'credit', 'duration', 'distance', 'commute_type', 'number')
