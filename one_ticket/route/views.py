from datetime import datetime, timedelta
from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, viewsets, filters, mixins, generics
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated

from . import serializers, models


class RouteGeneratorViewSet(viewsets.GenericViewSet,
                          mixins.CreateModelMixin):

    queryset = models.RouteGenerator.objects.all()
    serializer_class = serializers.RouteGeneratorSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        return models.RouteGenerator.objects.filter(user_id=user)

    def perform_create(self, serializer):
        """Sets the user to logged in user"""

        serializer.save(user_id=self.request.user)


class RoutesViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = models.Route.objects.all()
    serializer_class = serializers.RouteSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        options = models.RouteGenerator.objects.filter(user_id=user)
        return models.Route.objects.filter(option_id__in=options)

class RouteStepsViewSet(viewsets.ReadOnlyModelViewSet):
    
    queryset = models.RouteStep.objects.all()
    serializer_class = serializers.RouteStepSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        options = models.RouteGenerator.objects.filter(user_id=user)
        routes = models.Route.objects.filter(option_id__in=options)
        return models.RouteStep.objects.filter(route_id__in=routes)
