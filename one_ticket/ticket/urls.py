from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views


router = DefaultRouter()
router.register('my_one_tickets', views.MyOneTicketsViewSet)
router.register('my_tickets', views.MyTicketsViewSet)
router.register('verify', views.VerifyTicketViewSet)


urlpatterns = [
    path('', include(router.urls))
]
