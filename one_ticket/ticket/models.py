import uuid
from django.db import models

from profiles.models import UserProfile


class OneTicket(models.Model):
    """One ticket model"""

    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    option_id = models.IntegerField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    expiry = models.IntegerField(default=3600*24)
    active = models.BooleanField(default=True)
    secret = models.TextField(blank=False, max_length=32, unique=True)
    credit = models.IntegerField(blank=True)

    class Meta:
        ordering = ('user_id', 'id')

    def __str__(self):
        return str(self.id)

class Ticket(models.Model):
    """Ticket model"""

    COMMUTE_TYPES = {
        'bus': 'Bus',
        'metro': 'Metro',
        'cab': 'Cab',
        'train': 'Train'
    }

    STATES = {
        'active': 'Active',
        'used': 'Used',
        'cancelled': 'Cancelled',
        'expired': 'Expired',
        'refunded': 'Refunded'
    }

    id = models.AutoField(primary_key=True)
    one_ticket_id = models.ForeignKey(
        OneTicket, related_name='child_tickets', on_delete=models.CASCADE)
    source_lat = models.FloatField(blank=False)
    source_lng = models.FloatField(blank=False)
    source_name = models.TextField()
    destination_lat = models.FloatField(blank=False)
    destination_lng = models.FloatField(blank=False)
    destination_name = models.TextField()
    commute_type = models.TextField(blank=False, choices=COMMUTE_TYPES.items())
    credit = models.FloatField(blank=False)
    state = models.TextField(blank=False, choices=STATES.items())

    class Meta:
        ordering = ('one_ticket_id', 'id')

    def __str__(self):
        return '{}/{} {} -> {}'.format(
            self.one_ticket_id, self.id, self.source_name, self.destination_name
        )
