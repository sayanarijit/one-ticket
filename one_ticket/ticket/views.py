import uuid
from datetime import datetime, timedelta
from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, viewsets, filters, mixins, generics
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated

from . import serializers, models, permissions


class MyOneTicketsViewSet(viewsets.GenericViewSet,
                          mixins.CreateModelMixin,
                          mixins.ListModelMixin,
                          mixins.RetrieveModelMixin,
                          mixins.DestroyModelMixin):
    """Handles displaying one tickets of user"""

    queryset = models.OneTicket.objects.all()
    serializer_class = serializers.OneTicketSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,
                          permissions.ManageOwnOneTicket,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('id',)

    def get_queryset(self):
        user = self.request.user
        return models.OneTicket.objects.filter(user_id=user)

    def perform_create(self, serializer):
        """Sets the user to logged in user"""

        serializer.save(user_id=self.request.user)

class MyTicketsViewSet(viewsets.ReadOnlyModelViewSet):
    """Handles displaying active child tickets of user"""

    queryset = models.Ticket.objects.all()
    serializer_class = serializers.TicketSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,
                          permissions.ManageOwnTicket,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('id',)

    def get_queryset(self):
        user = self.request.user
        one_tickets = models.OneTicket.objects.filter(
            user_id=user)
        return models.Ticket.objects.filter(one_ticket_id__in=one_tickets)


class VerifyTicketViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    """Handles validation of tickets one tickets of user"""
    
    queryset = models.OneTicket.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,
                          permissions.VerifyUserTicket,)

    def get_queryset(self):
        user = self.request.user
        return models.OneTicket.objects.all()

    def retrieve(self, request, pk=None):
        ticket = models.OneTicket.objects.get(id=pk)
        if not ticket:
            return Response('not found', status=status.HTTP_404_NOT_FOUND)

        if ticket.secret != request.GET.get('secret', 'NAN'):
            return Response('rejected', status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_202_ACCEPTED)
