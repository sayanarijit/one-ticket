import uuid
from rest_framework import serializers

from . import models
from route.models import Route, RouteGenerator, RouteStep


class OneTicketSerializer(serializers.ModelSerializer):
    """A serializer for OneTicket object"""

    child_tickets = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = models.OneTicket
        fields = ('id', 'user_id', 'option_id', 'created_at', 'expiry',
                  'active', 'secret', 'credit', 'child_tickets')
        read_only_fields = ('id', 'user_id', 'created_at', 'expiry',
                            'active', 'secret', 'credit', 'child_tickets')
        extra_kwargs = {
            'option_id': {'write_only': True},
        }

    def create(self, validated_data):
        """Create and return new user"""

        user = models.UserProfile.object.get(username=validated_data['user_id'])
        generated = RouteGenerator.objects.get(user_id=user)
        route = Route.objects.get(id=validated_data['option_id'], option_id=generated)

        if user.credit < route.credit:
            raise serializers.ValidationError("insufficient credit.")

        user.credit -= route.credit

        one_ticket = models.OneTicket(
            user_id=user,
            credit=route.credit,
            option_id=generated.id,
            secret=uuid.uuid4().hex
        )
        user.save()
        one_ticket.save()
        
        steps = RouteStep.objects.filter(route_id=route.id)
        for step in steps:
            ticket = models.Ticket(
                one_ticket_id=one_ticket,
                source_lat=step.source_lat,
                source_lng=step.source_lng,
                source_name=step.source_name,
                destination_lat=step.destination_lat,
                destination_lng=step.destination_lng,
                destination_name=step.destination_name,
                commute_type=step.commute_type,
                credit=step.credit,
                state='active'
            )
            ticket.save()

        return one_ticket


class TicketSerializer(serializers.ModelSerializer):
    """A serializer for Ticket object"""

    class Meta:
        model = models.Ticket
        fields = ('id', 'one_ticket_id', 'source_lat', 'source_lng',
                  'source_name', 'destination_lat', 'destination_lng',
                  'destination_name', 'commute_type', 'credit', 'state')
        read_only_fields = ('id', 'one_ticket_id', 'source_lat', 'source_lng',
                            'source_name', 'destination_lat', 'destination_lng',
                            'destination_name', 'commute_type', 'credit')

