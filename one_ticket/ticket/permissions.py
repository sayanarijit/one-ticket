from rest_framework import permissions


class ManageOwnOneTicket(permissions.BasePermission):
    """Allow users to manage their own ticket"""

    def has_object_permission(self, request, view, obj):
        """Check user is trying to manage their own ticket"""

        return obj.user_id == request.user

class ManageOwnTicket(permissions.BasePermission):
    """Allow users to manage their own ticket"""

    def has_object_permission(self, request, view, obj):
        """Check user is trying to manage their own ticket"""

        return obj.one_ticket_id.user_id == request.user


class VerifyUserTicket(permissions.BasePermission):
    """Allow vendors to verify user tickets"""

    def has_object_permission(self, request, view, obj):
        """Check user is trying to manage their own ticket"""

        return request.user.account_type == 'vendor'
