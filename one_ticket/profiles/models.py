from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
    BaseUserManager
)


class UserProfileManager(BaseUserManager):
    """Custom user profile manager"""

    def create_user(self, username, email, name, account_type, password=None):
        """Creates a new profile object"""

        if not email:
            raise ValueError('User must have email address')

        email = self.normalize_email(email)
        user = self.model(username=username, email=email,
                          name=name, account_type=account_type)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, name, password):
        """Creates and saves new superuser"""

        user = self.create_user(username, email, name, 'passenger', password)

        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)


class UserProfile(AbstractBaseUser, PermissionsMixin):
    """User profile"""

    class Meta:
       ordering = ['username']
    
    ACCOUNT_TYPES = {
        'passenger': 'Passenger',
        'vendor': 'Vendor'
    }

    username = models.CharField(max_length=255, primary_key=True)
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    account_type = models.CharField(max_length=255,
                                    choices=ACCOUNT_TYPES.items(), default='passenger')
    credit = models.FloatField(default=0.0)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)

    object = UserProfileManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['name', 'email', 'account_type']

    def get_full_name(self):
        """Used to get a user full name"""
        return self.name

    def get_short_name(self):
        """Used to get a user's short name"""
        return self.name

    def __str__(self):
        return self.username
