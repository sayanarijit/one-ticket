from rest_framework import serializers

from . import models


class UserProfileSerializer(serializers.ModelSerializer):
    """A serializer for user profile object"""

    class Meta:
        model = models.UserProfile

        fields = ('username', 'email', 'name', 'password',
                  'account_type', 'credit', 'is_active', 'is_staff')
        read_only_fields = ('credit', 'is_active', 'is_staff')
        # fields = '__all__'
        extra_kwargs = {
            'password': {'write_only': True},
        }

    def create(self, validated_data):
        """Create and return new user"""

        user = models.UserProfile(
            username=validated_data['username'],
            email=validated_data['email'],
            name=validated_data['name'],
            account_type=validated_data['account_type'],
            credit=validated_data['credit']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user
