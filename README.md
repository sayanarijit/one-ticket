- [One Ticket Backend](#one-ticket-backend)
    - [Live demo](#live-demo)
        - [oneticket.ml/api](#oneticketmlapi)
    - [Installation](#installation)
        - [Requirements](#requirements)
        - [Setup](#setup)
        - [Run](#run)
    - [API docs](#api-docs)
        - [Postman API documentation](#postman-api-documentation)
        - [High level Work flow](#high-level-work-flow)
            - [For passengers](#for-passengers)
            - [For vendors](#for-vendors)

# One Ticket Backend

## Live demo

### [oneticket.ml/api](http://oneticket.ml/api)

| Username | Password   |
| -------- | ---------- |
| `admin`  | `password` |


## Installation

### Requirements

* Python3 (tested on python 3.7)


### Setup

```bash
git clone https://bitbucket.org/sayanarijit/one-ticket.git

cd one-ticket

pip install -r requirements.txt
```

### Run

```bash
python one_ticket/manage.py runserver '0.0.0.0:8080'
```


## API docs

### Postman API documentation

[documenter.getpostman.com/view/3444516/RWTsqasS](https://documenter.getpostman.com/view/3444516/RWTsqasS)

### High level Work flow

#### For passengers
[![passenger work fkow](https://raw.githubusercontent.com/sayanarijit/.files/master/One%20Ticket%20backend%20workflow%20for%20passengers%20and%20vendors-Passenger%20workflow.png)](https://goo.gl/gB4Nim)

#### For vendors
[![vendor work flow](https://raw.githubusercontent.com/sayanarijit/.files/master/One%20Ticket%20backend%20workflow%20for%20passengers%20and%20vendors-Vendor%20workflow.png)](https://goo.gl/gB4Nim)